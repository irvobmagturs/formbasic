import gql from "graphql-tag";

const queries = {
    isProposalSent: gql` query isProposalSent ($Proposal: ID!){
    Agreement(proposal:$Proposal){
        isSent
        sentOn @include(if: isSent) }
    }`
    ,
    isProposalConfirmed: gql` query isProposalConfirmed ($Proposal: ID!){
    Agreement(proposal:$Proposal){
        isConfirmed
        confirmedOn @include(if: isConfirmed) }
    }`,
    createProposal: gql`mutation createProposal ($arg2: ID!) {
    ...
  }`,
    typeProposal: gql`type Proposal{
    proposalHash: ID!
    email: String!
    file: File!
    info: [KeyValuePair!]!
    }`,
    typePair: gql`type KeyValuePair{ 
    attribute: String!
    value: String!
    }`,
    typeFile: gql`type File{
    fileContentHash: ID!
    fileName: String!
    fileType: String!
    }`,
    typeAgreement: gql`type Agreement{
    proposal: Proposal!
    sentOn: String
    isSent: Boolean!
    confirmedOn: String
    isConfirmed: Boolean!
    }`
};