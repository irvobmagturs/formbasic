<?php


namespace BhanviJain\OnlineAgreement;

use Kepawni\Twilted\Basic\ImmutableValue;

/**
 * @property-read string $host
 * @property-read int $port
 * @property-read string $username
 * @property-read string $password
 * @property-read bool $useSsl
 * @property-read bool $useTls
 *
 * @method self withHost(string $host)
 * @method self withPort(int $p)
 * @method self withUsername(string $user)
 * @method self withPassword(string $pass)
 * @method self withUseSsl(bool $ssl)
 * @method self withUseTls(bool $tls)
 *
 */
class SmtpSettings extends ImmutableValue
{
    /**
     * @param string $host
     * @param int $port
     * @param string $username
     * @param string $password
     * @param bool $useSsl
     * @param bool $useTls
     */
    function __construct(string $host, int $port, string $username, string $password, bool $useSsl=true, bool $useTls=false)
    {
        $this->init('host', $host);
        $this->init('port', $port);
        $this->init('username', $username);
        $this->init('password', $password);
        $this->init('useSsl', $useSsl);
        $this->init('useTls',$useTls);
    }
}