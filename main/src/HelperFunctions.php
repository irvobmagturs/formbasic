<?php

use BhanviJain\OnlineAgreement\EmailAttachment;
use BhanviJain\OnlineAgreement\EmailBody;
use BhanviJain\OnlineAgreement\EmailStructure;
use BhanviJain\OnlineAgreement\MapInfo;
use BhanviJain\OnlineAgreement\SmtpSettings;
use PHPMailer\PHPMailer\PHPMailer;
use function Ramsey\Uuid\v5;


/**
 *
 * @param PHPMailer $mailer
 * @param SmtpSettings $settings
 * @param EmailStructure $email
 * @return PHPMailer
 * @throws \PHPMailer\PHPMailer\Exception
 */


function writeNewEmail(PHPMailer $mailer, SmtpSettings $settings, EmailStructure $email)
{
    // smtp settings
    $mailer->isSMTP();
    $mailer->Host = $settings->host;
    $mailer->SMTPAuth = true;
    $mailer->Username = $settings->username;
    $mailer->Password = $settings->password;
    if ($settings->useSsl)
        $mailer->SMTPSecure = PHPMailer::ENCRYPTION_SMTPS;
    elseif ($settings->useTls)
        $mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
    $mailer->Port = $settings->port;
    // email creation
    $mailer->setFrom($email->fromAddress);
    $mailer->addAddress($email->toAddress);
    $mailer->Subject = $email->subject;

    /**
     * @var EmailBody $body
     * @var EmailAttachment $attachment
     */
    //email attachment
    $attachment = $email->attachment;
    $file_path = $attachment->filePath;
    $file_name = $attachment->fileName;
    $encoding = $attachment->contentTransferEncoding;
    $mime_type = $attachment->mimeType;
    $mailer->AddAttachment($file_path, $file_name, $encoding, $mime_type);
    //email body
    $body = $email->body;
    $mailer->isHTML($body->isHtml);
    $mailer->Body = $body->message;
    return $mailer;
}

/**
 * @param string $rootUuid
 * @param string $fileContents
 * @param string $toAddress
 * @param string $additionalInfo
 * @return array
 */
function createProposal(string $rootUuid, string $fileContents, string $toAddress, string $additionalInfo)
{
    $FileHash = v5($rootUuid, $fileContents);
    return [v5(v5($FileHash, $toAddress),$additionalInfo) , $FileHash];
}
