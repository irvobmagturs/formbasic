<?php


namespace BhanviJain\OnlineAgreement;

use Kepawni\Twilted\Basic\ImmutableValue;

/**
 * @property-read string $filePath
 * @property-read string $fileName
 * @property-read string $mimeType
 * @property-read string $contentTransferEncoding
 *
 * @method self withFilePath(string $p)
 * @method self withFileName(string $f)
 * @method self withMimeType(string $t)
 * @method self withContentTransferEncoding(string $e)
 */
class EmailAttachment extends ImmutableValue
{
    /**
     * @param string $filePath
     * @param string $fileName
     * @param string $mimeType
     * @param string $encoding
     */
    function __construct(string $filePath, string $fileName, string $mimeType, string $encoding)
    {
        $this->init('filePath', $filePath);
        $this->init('fileName', $fileName);
        $this->init('mimeType', $mimeType);
        $this->init('contentTransferEncoding', $encoding);
    }

}