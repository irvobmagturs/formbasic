<?php


namespace BhanviJain\OnlineAgreement;


class MapInfo
{
    private array $info;
    private string $infoString;

    /**
     * AdditionalInfo constructor.
     * @param $keys
     * @param $values
     */
    function __construct($keys, $values)
    {
        $this->info = [];
        for ($i = 0; $i < count($keys); $i++)
            $this->info[trim($keys[$i])] = trim($values[$i]);
    }

    public function getInfo(): array
    {
        return $this->info;
    }

    public function getInfoString(): string
    {
        $this->setInfoString();
        return $this->infoString;
    }

    private function setInfoString(): void
    {
        $info = $this->info;
        ksort($info);
        $infoString = "";
        foreach ($info as $attribute => $value) {
            $infoString .= $attribute . ":" . $value . ",";
        }
        $this->infoString = $infoString;
    }

}