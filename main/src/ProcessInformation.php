<?php

use BhanviJain\OnlineAgreement\EmailAttachment;
use BhanviJain\OnlineAgreement\EmailBody;
use BhanviJain\OnlineAgreement\EmailStructure;
use BhanviJain\OnlineAgreement\MapInfo;
use BhanviJain\OnlineAgreement\ProposalCreationResult;
use BhanviJain\OnlineAgreement\SmtpSettings;
use Kepawni\Limerick\Hexastore;
use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__ . '/HelperFunctions.php';

/**
 * @param array $post
 * @param array $attachedFile
 * @param string $rootUuid
 * @param Hexastore $hexastore
 * @param SmtpSettings $smtpSettings
 * @param PHPMailer $mailer
 * @param string $baseUrl
 * @param string $fromAddress
 * @return ProposalCreationResult
 * @throws \PHPMailer\PHPMailer\Exception
 */
function processInformation(array $post, array $attachedFile,string $rootUuid ,Hexastore $hexastore,
                            SmtpSettings $smtpSettings, PHPMailer $mailer, string $baseUrl, string $fromAddress)
{
    // output variable
    $result = new ProposalCreationResult();
    // input information
    $toAddress = $post["emailid"];
    $file_tmp = $attachedFile["tmp_name"];
    $info = new MapInfo($post["infoKeys"], $post["infoValues"]);

    if (!empty($file_tmp)) {
        // uploaded file information
        $result = $result->withIsFileUploaded(true);
        $file_content = file_get_contents($attachedFile["tmp_name"]);
        $file_name = $attachedFile['name'];
        $type = $attachedFile['type']; // ToDo: mime type returned by browser

        // Create hashes
        [$Proposal,$FileHash] = createProposal($rootUuid,$file_content,$toAddress,$info->getInfoString());

        // store the date proposal was sent before (if sent before)
        foreach ($hexastore->find($Proposal, "sent on", null) as [$proposal, $predicate, $time])
            $result = $result->withPreviouslySentOn(new DateTime($time));

        if ($result->getPreviouslySentOn()) {
            // proposal already sent. hence do not send email again
            foreach ($hexastore->find($Proposal, "confirmed on", null) as [$f, $p, $time]) {
                // store date of confirmation (if confirmed)
                $result = $result->withConfirmedOn(new DateTime($time));
            }
        } else {
            $subject = "CONFIRMATION LINK";
            $body = new EmailBody(true, $baseUrl, $Proposal, $info->getInfo());
            $attachment = new EmailAttachment($file_tmp, $file_name, $type, PHPMailer::ENCODING_BASE64);
            $email = new EmailStructure($fromAddress, $toAddress, $subject, $body, $attachment);

            $mailer = writeNewEmail($mailer, $smtpSettings, $email);

            if ($mailer->send()) {
                $time = date(DATE_ATOM);
                $result = $result->withConfirmationUrlJustGenerated($baseUrl . "/confirm.php?proposal=" . $Proposal);
                //ToDo: are pair attributes need to be stored in the hexastore seperately?
                $hexastore->store($Proposal, "sent to", $toAddress);
                $hexastore->store($Proposal, "sent on", $time);
                $hexastore->store($Proposal, "refers to file", $FileHash);
                $hexastore->store($FileHash, "is of type", $type);
            } else
                $result = $result->withMailingError($mailer->ErrorInfo);
        }

    } else
        $result = $result->withIsFileUploaded(false);
    return $result;
}