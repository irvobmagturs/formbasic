<!DOCTYPE HTML>
<html lang="en">
<?php

use BhanviJain\OnlineAgreement\ProposalCreationResult;
use BhanviJain\OnlineAgreement\SmtpSettings;
use PHPMailer\PHPMailer\PHPMailer;

require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/useConfig.php';
require_once __DIR__ . '/src/ProposalCreationResult.php';
require_once __DIR__ . '/src/ProcessInformation.php';


[$hexastore, $redis] = include __DIR__ . '/lib/useHexastore.php';

/**
 * @var ProposalCreationResult $result
 */

if (isset($_POST["submit"])) {
    $mailer = new PHPMailer();
    $smtp = new SmtpSettings(SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD, SMTP_SSL, SMTP_TLS);
    $result = processInformation($_POST, $_FILES["attachment"], ROOT_UUID, $hexastore, $smtp, $mailer, BASE_URL, FROM_ADDRESS);
}
?>


<head>
    <meta charset="utf-8">
    <title>CONTACT FORM</title>
</head>
<body>
<h4>
    <?php
    if ($result->isFileUploaded()) {
        $message = $result->getMailingError() ? "MESSAGE NOT SENT. ERROR:" . $result->getMailingError() : "";
        $message = $result->getPreviouslySentOn() ? "THIS FILE HAS ALREADY BEEN SENT." : "The Proposal has been sent. Thank You!";
        $message .= $result->getConfirmedOn() ? "<br>This Proposal was confirmed on " . $result->getConfirmedOn()->format(DATE_ATOM) : "";
    } else
        $message = "NO FILE UPLOADED";
    echo $message;
    ?>
</h4>
</body>

</html>