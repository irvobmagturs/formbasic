<!DOCTYPE HTML>
<html lang="en">
<?php
use Kepawni\Limerick\Hexastore;
use Predis\Client;
require_once __DIR__ . '/vendor/autoload.php';
require_once __DIR__ . '/lib/useConfig.php';

/**
 * @var Hexastore $hexastore
 * @var Client $redis
 */
[$hexastore, $redis] = include __DIR__ . '/lib/useHexastore.php';

$proposal = $_GET['proposal'];
$time = date(DATE_ATOM);
$confirmed = iterator_count($hexastore->find($proposal,"confirmed on",null))>0;
if(!$confirmed){
    $hexastore->store($proposal,"confirmed on",$time);
//    $result = new ProposalCreationResult(); // ToDo: to be implemented for messages?
    $message ="THANK YOU FOR THE CONFIRMATION!";
}
else
    $message = "THIS PROPOSAL HAS ALREADY BEEN CONFIRMED."
?>
<head>
    <meta charset="utf-8">
    <title>CONFIRMED</title>
</head>
<body>
<h4><?php echo $message?></h4>
</body>

</html>
