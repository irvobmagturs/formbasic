<?php declare(strict_types=1);
use Kepawni\Limerick\Hexastore;
use Predis\Client;

$redis = new Client(REDIS_URI, ['parameters' => ['password' => REDIS_PASSWORD, 'database' => REDIS_DB_INDEX]]);
$hexastore = new Hexastore($redis, TRIPLE_KEY, TRIPLE_SEPARATOR, TRIPLE_ESCAPE);
return [$hexastore, $redis];
