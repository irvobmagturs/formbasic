<?php declare(strict_types=1);
//
// IMPORTANT
//
// Use this file only as a template for your own config.private.php
// Never share or publish your config.private.php
//
//use function Ramsey\Uuid\v4;

const FROM_ADDRESS='john_doe@example.com';
const SMTP_HOST='smtp.example.com';
const SMTP_PORT=465;
const SMTP_USER='john_doe';
const SMTP_PASSWORD='§€Cr3t';
const SMTP_SSL= true;
const SMTP_TLS = false;
//
const REDIS_URI = 'tcp://localhost:6379';
const REDIS_PASSWORD = null;
const REDIS_DB_INDEX = 0;
//
const TRIPLE_KEY = 'hexastore';
const TRIPLE_ESCAPE = '#';
const TRIPLE_SEPARATOR = ':';
//
const ROOT_UUID = "25366db3-e466-4de4-a7d4-e6d4e8e77aa1";
const BASE_URL = "http://localhost:80";