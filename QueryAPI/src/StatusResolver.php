<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementQueryAPI;

use Kepawni\Serge\Infrastructure\GraphQL\TypeResolver;

class StatusResolver extends TypeResolver
{
    public function __construct(TypeResolver $base = null)
    {
        parent::__construct($base);
        $this->addResolverForField('CqrsQuery', 'status', function () {
            return true;
        });
    }
}
