<?php declare(strict_types=1);
use BhanviJain\OnlineAgreementCommandAPI\Aggregate\Proposal;
use BhanviJain\OnlineAgreementCommandAPI\Handler\ProposalHandler;
use BhanviJain\OnlineAgreementCommandAPI\JsonEventStore;
use BhanviJain\OnlineAgreementCommandAPI\Projector\ProposalProjector;
use BhanviJain\OnlineAgreementCommandAPI\SimpleWhenMethodEventBus;
use Kepawni\Serge\Infrastructure\GraphQL\CqrsCommandBus;
use Kepawni\Twilted\Basic\SimpleRepository;
use Kepawni\Twilted\EventBus;

function addCommandHandlersToCommandBus(CqrsCommandBus $commandBus): void
{
    $eventStore = new JsonEventStore();
    $eventBus = addProjectorsToEventBus(new SimpleWhenMethodEventBus());
    $proposalRepository = new SimpleRepository(Proposal::class, $eventBus, $eventStore);
    // call append to add all your command handlers
    $commandBus->append(Proposal::class, new ProposalHandler($proposalRepository));
}

function addProjectorsToEventBus(SimpleWhenMethodEventBus $eventBus): EventBus
{
    // call registerHandler to add all your projectors
    $eventBus->registerHandler(new ProposalProjector());
    return $eventBus;
}
