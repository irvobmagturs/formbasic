<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI;

use RuntimeException;

class EmailIdErrorException extends RuntimeException
{
}
