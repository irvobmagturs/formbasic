<?php


namespace BhanviJain\OnlineAgreementCommandAPI;


use DateTimeImmutable;
use Kepawni\Twilted\Basic\SimpleDomainEvent;
use Kepawni\Twilted\Basic\SimpleEventStream;
use Kepawni\Twilted\DomainEvent;
use Kepawni\Twilted\EntityIdentifier;
use Kepawni\Twilted\EventPayload;
use Kepawni\Twilted\EventStore;
use Kepawni\Twilted\EventStream;

class JsonEventStore implements EventStore
{
    //private $events = [];
    const EVENT_STORE = __DIR__ . "/../data/eventstore";

    public function append(EventStream $events): void
    {
        foreach ($events as $recordedEvent) {
            $eventType = get_class($recordedEvent->getPayload());
            $aggregateIdString = $recordedEvent->getId()->fold();
            $dateString = $recordedEvent->getRecordedOn()->format(DATE_ATOM);
            $serializedEventData = $recordedEvent->getPayload()->windUp();
            // naively store the details in an array
            // $this->events[$aggregateIdString][] = [$eventType, $dateString, $serializedEventData];
            $filename = self::EVENT_STORE . "/" . $aggregateIdString . ".json";
            $data = file_exists($filename) ? json_decode(file_get_contents($filename), true) : [];
            $data[] = [$eventType, $dateString, $serializedEventData];
            file_put_contents($filename, json_encode($data,JSON_PRETTY_PRINT));
        }
    }

    public function retrieve(EntityIdentifier $id): EventStream
    {
        /** @var DomainEvent[] $domainEvents */
        $domainEvents = [];
        $filename = self::EVENT_STORE . "/" . $id->fold() . ".json";
        // load the domain events from somewhere
        $data = json_decode(file_get_contents($filename), true);
        /** @var EventPayload $eventType */
        foreach ($data as [$eventType, $dateString, $serializedEventData]) {
            $domainEvents[] = new SimpleDomainEvent(
                $eventType::unwind($serializedEventData),
                $id,
                new DateTimeImmutable($dateString)
            );
        }
        return new SimpleEventStream($domainEvents);
    }
}
