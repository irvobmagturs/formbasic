<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Event\Proposal;

use BhanviJain\OnlineAgreementCommandAPI\Value\File;
use Kepawni\Serge\Infrastructure\AbstractEventPayloadBase;
use Kepawni\Twilted\Windable;

/**
 * @property-read File $file
 */
class FileWasAttached extends AbstractEventPayloadBase
{
    /**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            File::unwind($spool[0])
        );
    }

public function __construct(File $file)
    {
        $this->init('file', $file);
    }

public function windUp(): array
    {
        return [
            $this->file->windUp()
        ];
    }
}
