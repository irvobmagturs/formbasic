<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Projector;

use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasEstablished;
use Kepawni\Twilted\DomainEvent;

class ProposalProjector
{
    public function __construct(/* add arguments for all required dependencies */) {
        // set up the instance
    }

    public function whenProposalWasEstablished(ProposalWasEstablished $payload, DomainEvent $event): void {
        // here's how to access the data (PHP's intellisense will help you):
        //
        // $aggregateId = $event->getId();
        // $eventDateTime = $event->getRecordedOn();
        //
        // $emailId = $payload->emailId;
        //
        // do something with it
    }
    
    // add more when... methods for all the other events
}
