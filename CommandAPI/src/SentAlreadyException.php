<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI;

use RuntimeException;

class SentAlreadyException extends RuntimeException
{
}
