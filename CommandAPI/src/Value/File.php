<?php declare(strict_types=1);
namespace BhanviJain\OnlineAgreementCommandAPI\Value;

use Kepawni\Serge\Infrastructure\AbstractValueObjectBase;
use Kepawni\Twilted\Basic\AggregateUuid;
use Kepawni\Twilted\Windable;

/**
 * @property-read AggregateUuid $fileContentHash
 * @property-read string $fileName
 * @property-read string $fileType
 * @method self withFileContentHash(AggregateUuid $v)
 * @method self withFileName(string $v)
 * @method self withFileType(string $v)
 */
class File extends AbstractValueObjectBase
{
    /**
 * @param array $map
 * @return static
 */
public static function fromHashMap(array $map): AbstractValueObjectBase
    {
        return new self(
            AggregateUuid::unfold($map['fileContentHash']),
            strval($map['fileName']),
            strval($map['fileType'])
        );
    }

/**
 * @param array $spool
 * @return static
 */
public static function unwind(array $spool): Windable
    {
        return new self(
            AggregateUuid::unfold($spool[0]),
            strval($spool[1]),
            strval($spool[2])
        );
    }

public function __construct(AggregateUuid $fileContentHash, string $fileName, string $fileType)
    {
        $this->init('fileContentHash', $fileContentHash);
        $this->init('fileName', $fileName);
        $this->init('fileType', $fileType);
    }

public function windUp(): array
    {
        return [
            $this->fileContentHash->fold(),
            $this->fileName,
            $this->fileType
        ];
    }
}
