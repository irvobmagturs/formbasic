<?php declare(strict_types=1);

namespace BhanviJain\OnlineAgreementCommandAPI\Aggregate;

use BhanviJain\OnlineAgreementCommandAPI\ForbiddenFileTypeException;
use BhanviJain\OnlineAgreementCommandAPI\EmailIdErrorException;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\FileWasAttached;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\InfoWasProvided;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasEstablished;
use BhanviJain\OnlineAgreementCommandAPI\Event\Proposal\ProposalWasSent;
use BhanviJain\OnlineAgreementCommandAPI\FileNotAttachedException;
use BhanviJain\OnlineAgreementCommandAPI\ProposalNotEstablishedException;
use BhanviJain\OnlineAgreementCommandAPI\SentAlreadyException;
use BhanviJain\OnlineAgreementCommandAPI\Value\File;
use BhanviJain\OnlineAgreementCommandAPI\Value\KeyValuePair;
use Kepawni\Twilted\Basic\SimpleAggregateRoot;
use Kepawni\Twilted\EntityIdentifier;

class Proposal extends SimpleAggregateRoot
{
    private bool $hasBeenSentAlready = false;
    private bool $isFileAttached = false;
    private bool $isEstablished = false;

    public static function establish(EntityIdentifier $proposalId, string $emailId): self
    {
        self::guardEmailIdIsSyntacticallyCorrect($emailId);
        $newProposal = new static($proposalId);
        $newProposal->recordThat(
            new ProposalWasEstablished($emailId)
        );
        return $newProposal;
    }

    public function attachFile(File $file): void
    {
        $this->guardIsAlreadySent();
        $this->guardFileIsAllowed($file);
        $this->recordThat(new FileWasAttached($file));
    }

    public function provideInfo(KeyValuePair $attribute): void
    {
        // $this->guardAttributeIsSupported($attribute);
        $this->guardIsAlreadySent();
        $this->recordThat(new InfoWasProvided($attribute));
    }

    public function send(): void
    {
        $this->guardProposalEstablished();
        $this->guardFileIsAttached();
        $this->guardIsAlreadySent();
        $this->recordThat(new ProposalWasSent());
    }

    protected function whenFileWasAttached(FileWasAttached $event): void
    {
        $this->isFileAttached = true;
    }

    protected function whenInfoWasProvided(InfoWasProvided $event): void
    {
    }

    protected function whenProposalWasEstablished(ProposalWasEstablished $event): void
    {
        $this->isEstablished = true;
    }

    protected function whenProposalWasSent(ProposalWasSent $event): void
    {
        $this->hasBeenSentAlready = true;
    }

    private static function guardEmailIdIsSyntacticallyCorrect(string $emailId)
    {
        //ToDo: one @ symbol(not in first or last of the string)if not throw an exception (EmailIdErrorException)
        if (!filter_var($emailId, FILTER_VALIDATE_EMAIL))
            throw new EmailIdErrorException("Invalid Email ID");
    }

    private function guardProposalEstablished()
    {
        if (!$this->isEstablished)
            throw new ProposalNotEstablishedException("This proposal has not been established.");
    }

    private function guardFileIsAllowed(File $file): void
    {
        //ToDo: pdf,docx,xlsx,odf,png,gif,jpeg - throw exception
        $type = $file->fileType;
        $allowedTypes = ["application/pdf", "application/msword", "application/excel",
            "application/vnd.ms-excel", "application/x-excel", "application/x-msexcel", "image/png",
            "image/gif", "image/jpeg", "image/pjpeg", "application/vnd.oasis.opendocument.formula"];
        foreach ($allowedTypes as $t) {
            if ($type == $t)
                return;
        }
        throw new ForbiddenFileTypeException("This file type is not allowed");
    }

    private function guardFileIsAttached(): void
    {
        if (!$this->isFileAttached)
            throw new FileNotAttachedException("No File Attached.");

    }

    private
    function guardIsAlreadySent()
    {
        if ($this->hasBeenSentAlready)
            throw new SentAlreadyException("This proposal has already been sent");
    }
}
